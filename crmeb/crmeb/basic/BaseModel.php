<?php


namespace crmeb\basic;

use crmeb\traits\ModelTrait;
use think\Model;

/**
 * Class BaseModel
 * @package crmeb\basic
 * @mixin ModelTrait
 */
class BaseModel extends Model
{

}
