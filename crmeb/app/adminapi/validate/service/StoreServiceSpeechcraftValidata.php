<?php


namespace app\adminapi\validate\service;


use think\Validate;

class StoreServiceSpeechcraftValidata extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'message' => 'require',
        'sort' => 'require|number',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'message.require' => '请填写话术内容',
        'sort.require' => '请填写排序数字',
        'sort.number' => '排序序号为整数',
    ];
}
