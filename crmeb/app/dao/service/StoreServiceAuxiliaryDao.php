<?php


namespace app\dao\service;


use app\dao\other\AuxiliaryDao;

/**
 * 客服辅助表
 * Class StoreServiceAuxiliaryDao
 * @package app\dao\service
 */
class StoreServiceAuxiliaryDao extends AuxiliaryDao
{

    /**
     * 搜索
     * @param array $where
     * @return \crmeb\basic\BaseModel|mixed|\think\Model
     */
    protected function search(array $where = [])
    {
        return parent::search($where)->where('type', 0);
    }

}
