<?php


namespace app\dao\service;


use app\dao\BaseDao;
use app\model\service\StoreServiceSpeechcraft;

/**
 * 客服话术dao
 * Class StoreServiceSpeechcraftDao
 * @package app\dao\service
 */
class StoreServiceSpeechcraftDao extends BaseDao
{
    /**
     * 设置模型
     * @return string
     */
    protected function setModel(): string
    {
        return StoreServiceSpeechcraft::class;
    }

    /**
     * 获取话术列表
     * @param array $where
     * @param int $page
     * @param int $limit
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getSpeechcraftList(array $where, int $page, int $limit)
    {
        return $this->search($where)->with(['cateName'])->when($page && $limit, function ($query) use ($page, $limit) {
            $query->page($page, $limit);
        })->order('sort DESC')->select()->toArray();
    }
}
