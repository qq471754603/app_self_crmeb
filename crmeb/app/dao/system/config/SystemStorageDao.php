<?php


namespace app\dao\system\config;


use app\dao\BaseDao;
use app\model\system\config\SystemStorage;
use crmeb\traits\SearchDaoTrait;

/**
 * Class SystemStorageDao
 * @package app\dao\system\config
 */
class SystemStorageDao extends BaseDao
{

    use SearchDaoTrait;

    /**
     * @return string
     */
    protected function setModel(): string
    {
        return SystemStorage::class;
    }

    /**
     * @param array $where
     * @return \crmeb\basic\BaseModel|mixed|\think\Model
     */
    public function search(array $where = [])
    {
        return parent::search($where)->when(isset($where['type']), function ($query) use ($where) {
            $query->where('type', $where['type']);
        })->where('is_delete', 0)->when(isset($where['access_key']), function ($query) use ($where) {
            $query->where('access_key', $where['access_key']);
        });
    }
}
