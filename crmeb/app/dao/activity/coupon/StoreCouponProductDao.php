<?php

declare (strict_types=1);

namespace app\dao\activity\coupon;

use app\dao\BaseDao;
use app\model\activity\coupon\StoreCouponProduct;

/**
 *
 * Class StoreCouponProductDao
 * @package app\dao\coupon
 */
class StoreCouponProductDao extends BaseDao
{

    /**
     * 设置模型
     * @return string
     */
    protected function setModel(): string
    {
        return StoreCouponProduct::class;
    }

}
