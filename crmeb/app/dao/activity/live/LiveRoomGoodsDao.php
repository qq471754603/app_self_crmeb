<?php

declare (strict_types=1);

namespace app\dao\activity\live;


use app\dao\BaseDao;
use app\model\activity\live\LiveRoomGoods;


class LiveRoomGoodsDao extends BaseDao
{


    protected function setModel(): string
    {
        return LiveRoomGoods::class;
    }

    public function clear($id)
    {
        return $this->getModel()->where('live_room_id', $id)->delete();
    }
}
