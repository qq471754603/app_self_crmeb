<?php


namespace app\dao\user;


use app\dao\BaseDao;
use app\model\user\UserLabelCate;

/**
 * Class UserLabelCateDao
 * @package app\dao\user
 */
class UserLabelCateDao extends BaseDao
{

    /**
     * 设置模型
     * @return string
     */
    protected function setModel(): string
    {
        return UserLabelCate::class;
    }

    /**
     * 获取标签分类列表
     * @param array $where
     * @param int $page
     * @param int $limit
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getLabelList(array $where, int $page, int $limit)
    {
        return $this->search($where)->when($page && $limit, function ($query) use ($page, $limit) {
            $query->page($page, $limit);
        })->order('sort DESC')->select()->toArray();
    }

    /**
     * 获取全部标签分类
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getAll(array $with = [])
    {
        return $this->getModel()->when(count($with), function ($query) use ($with) {
            $query->with($with);
        })->order('sort DESC')->select()->toArray();
    }
}
