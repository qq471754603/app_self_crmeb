<?php


namespace app\dao\user;


use app\dao\BaseDao;
use app\model\user\MemberRight;

class MemberRightDao extends BaseDao
{
    /** 设置模型
     * @return string
     */
    protected function setModel(): string
    {
        // TODO: Implement setModel() method.
        return MemberRight::class;
    }

    /**获取会员权益接口
     * @param array $where
     * @param int $page
     * @param int $limit
     * @param array $field
     * @return \think\Collection
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getSearchList(array $where, int $page = 0, int $limit = 0, array $field = ['*'])
    {
        return $this->search($where)->order('sort desc,id desc')
            ->field($field)
            ->when($page && $limit, function($query) use($page, $limit){
                $query->page($page, $limit);
            })
            ->select()->toArray();

    }



}
