<?php


namespace app\dao\order;


use app\dao\BaseDao;
use app\model\order\StoreOrderEconomize;

/**
 * Class StoreOrderEconomizeDao
 * @package app\dao\order
 */
class StoreOrderEconomizeDao extends BaseDao
{

    protected function setModel(): string
    {
        return StoreOrderEconomize::class;
    }

    /**
     * @param array $where
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList(array $where)
    {
        return $this->getModel()->where($where)->select()->toArray();
    }

}
