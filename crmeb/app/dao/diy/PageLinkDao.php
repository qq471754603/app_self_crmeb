<?php

declare (strict_types=1);

namespace app\dao\diy;

use app\dao\BaseDao;
use app\model\diy\PageLink;

/**
 * 页面链接
 * Class PageLinkDao
 * @package app\dao\diy
 */
class PageLinkDao extends BaseDao
{

    /**
     * 设置模型
     * @return string
     */
    protected function setModel(): string
    {
        return PageLink::class;
    }

    /**
     * 获取列表
     * @param array $where
     * @param string $field
     * @param int $page
     * @param int $limit
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getList(array $where, string $field = '*', int $page = 0, int $limit = 0)
    {
        return $this->search($where)->field($field)->when($page && $limit, function ($query) use ($page, $limit) {
            $query->page();
        })->order('sort desc,id asc')->select()->toArray();
    }
}
