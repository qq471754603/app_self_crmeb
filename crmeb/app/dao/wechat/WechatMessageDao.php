<?php


namespace app\dao\wechat;


use app\dao\BaseDao;
use app\model\wechat\WechatMessage;

/**
 * Class WechatMessageDao
 * @package app\dao\wechat
 */
class WechatMessageDao extends BaseDao
{
    /**
     * @return string
     */
    protected function setModel(): string
    {
        return WechatMessage::class;
    }
}
