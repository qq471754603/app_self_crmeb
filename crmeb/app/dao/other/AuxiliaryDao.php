<?php


namespace app\dao\other;


use app\dao\BaseDao;
use app\model\other\Auxiliary;

/**
 * 辅助表
 * Class AuxiliaryDao
 * @package app\dao\other
 */
class AuxiliaryDao extends BaseDao
{

    /**
     * @return string
     */
    protected function setModel(): string
    {
        return Auxiliary::class;
    }

}
