<?php


namespace app\http\middleware;


use app\Request;
use crmeb\interfaces\MiddlewareInterface;

/**
 * Class BaseMiddleware
 * @package app\api\middleware
 */
class BaseMiddleware implements MiddlewareInterface
{
    public function handle(Request $request, \Closure $next, bool $force = true)
    {
        if (!Request::hasMacro('uid')) {
            Request::macro('uid', function(){ return 0; });
        }
        if (!Request::hasMacro('adminId')) {
            Request::macro('adminId', function(){ return 0; });
        }
        if (!Request::hasMacro('kefuId')) {
            Request::macro('kefuId', function(){ return 0; });
        }

        return $next($request);
    }
}
