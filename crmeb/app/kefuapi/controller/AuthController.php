<?php


namespace app\kefuapi\controller;


use crmeb\basic\BaseController;

/**
 * Class AuthController
 * @package app\kefuapi\controller
 */
abstract class AuthController extends BaseController
{

    /**
     * @var int
     */
    protected $kefuId;

    /**
     * @var array
     */
    protected $kefuInfo;

    /**
     * 初始化
     */
    protected function initialize()
    {
        $this->kefuId = $this->request->kefuId();
        $this->kefuInfo = $this->request->kefuInfo();
    }
}
