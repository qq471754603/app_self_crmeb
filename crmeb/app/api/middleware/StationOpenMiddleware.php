<?php


namespace app\api\middleware;


use app\Request;
use crmeb\interfaces\MiddlewareInterface;

/**
 * Class StationOpenMiddleware
 * @package app\api\middleware
 */
class StationOpenMiddleware implements MiddlewareInterface
{
    public function handle(Request $request, \Closure $next)
    {
        if (!sys_config('station_open', 1)) {
            return app('json')->make('410010', '站点升级中，请稍候访问');
        }
        return $next($request);
    }
}
