<?php


namespace app\api\controller\v1\activity;

use app\Request;
use app\services\activity\advance\StoreAdvanceServices;

/**
 * 预售控制器
 * Class StoreAdvanceController
 * @package app\api\controller\v1\activity
 */
class StoreAdvanceController
{
    /**
     * StoreAdvanceController constructor.
     * @param StoreAdvanceServices $services
     */
    public function __construct(StoreAdvanceServices $services)
    {
        $this->services = $services;
    }

    /**
     * 预售列表
     * @param Request $request
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index(Request $request)
    {
        $where = $request->getMore([
            ['time_type', 0]
        ]);
        $where['status'] = 1;
        $data = $this->services->getList($where);
        return app('json')->successful($data);
    }

    /**
     * 预售商品详情
     * @param Request $request
     * @param $id
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function detail(Request $request, $id)
    {
        $data = $this->services->getAdvanceinfo($request, $id);
        return app('json')->successful($data);
    }
}
