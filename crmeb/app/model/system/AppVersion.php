<?php



namespace app\model\system;

use crmeb\basic\BaseModel;
use crmeb\traits\ModelTrait;

/**
 * Class AppVersion
 * @package app\model\system
 */
class AppVersion extends BaseModel
{
    use ModelTrait;

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'app_version';

    public function searchPlatformAttr($query, $value, $data)
    {
        if($value!='') $query->where('platform',$value);
    }
}
