<?php


namespace app\model\system\config;


use crmeb\basic\BaseModel;

/**
 * 云存储
 * Class SystemStorage
 * @package app\model\system\config
 */
class SystemStorage extends BaseModel
{

    /**
     * @var string
     */
    protected $name = 'system_storage';

    /**
     * @var string
     */
    protected $pk = 'id';

    /**
     * @var bool
     */
    protected $autoWriteTimestamp = false;

    /**
     * @param $query
     * @param $value
     */
    public function searchNameAttr($query, $value)
    {
        $query->where('name', $value);
    }

    /**
     * 类型搜索器
     * @param $query
     * @param $value
     */
    public function searchTypeAttr($query, $value)
    {
        if ($value) $query->where('type', $value);
    }

    /**
     * 状态搜索器
     * @param $query
     * @param $value
     */
    public function searchStatusAttr($query, $value)
    {
        if ($value !== '') $query->where('status', $value);
    }
}
