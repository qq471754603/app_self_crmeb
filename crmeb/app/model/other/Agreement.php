<?php

namespace app\model\other;

use crmeb\traits\ModelTrait;
use crmeb\basic\BaseModel;

/**
 *  协议Model
 * Class Cache
 * @package app\model\other
 */
class Agreement extends BaseModel
{
    use ModelTrait;

    const EXPIRE = 0;
    /**
     * 模型名称
     * @var string
     */
    protected $name = 'agreement';
}
