<?php


namespace app\model\activity\live;


use crmeb\basic\BaseModel;
use crmeb\traits\ModelTrait;

/**
 * Class LiveRoomGoods
 * @package app\model\live
 */
class LiveRoomGoods extends BaseModel
{
    use ModelTrait;

    protected $name = 'live_room_goods';

    public function goods()
    {
        return $this->hasOne(LiveGoods::class, 'id', 'live_goods_id');
    }

    public function room()
    {
        return $this->hasOne(LiveRoom::class, 'id', 'live_room_id');
    }
}
