<?php


namespace app\model\user;


use crmeb\basic\BaseModel;
use think\Model;

/**
 * Class UserLabelCate
 * @package app\model\user
 */
class UserLabelCate extends BaseModel
{
    /**
     * 表名
     * @var string
     */
    protected $name = 'user_label_cate';

    /**
     * 主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * @param Model $query
     * @param $value
     */
    public function searchNameAttr($query, $value)
    {
        $query->whereLike('name', '%' . $value . '%');
    }

    /**
     * 一对多关联
     * @return \think\model\relation\HasMany
     */
    public function label()
    {
        return $this->hasMany(UserLabel::class, 'label_cate', 'id');
    }
}
