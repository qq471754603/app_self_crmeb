<?php


namespace app\model\wechat;


use app\model\user\User;
use crmeb\basic\BaseModel;
use crmeb\traits\ModelTrait;

/**
 * 渠道码model
 * Class WechatKey
 * @package app\model\wechat
 */
class WechatQrcode extends BaseModel
{
    use ModelTrait;

    /**
     * 数据表主键
     * @var string
     */
    protected $pk = 'id';

    /**
     * 模型名称
     * @var string
     */
    protected $name = 'wechat_qrcode';

    public function user()
    {
        return $this->hasOne(User::class, 'uid', 'uid')->bind(['nickname', 'avatar']);
    }

    public function record()
    {
        return $this->hasOne(WechatQrcodeRecord::class, 'qid', 'id');
    }

    public function searchUidAttr($query, $value)
    {
        if ($value != '') $query->where('uid', $value);
    }

    public function searchCateIdAttr($query, $value)
    {
        if ($value != '') $query->where('cate_id', $value);
    }

    public function searchNameAttr($query, $value)
    {
        if ($value != '') $query->where('name', $value);
    }

    public function searchIsDelAttr($query, $value)
    {
        if ($value !== '') $query->where('is_del', $value);
    }
}
