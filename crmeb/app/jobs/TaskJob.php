<?php


namespace app\jobs;

use app\services\message\sms\SmsRecordServices;
use app\services\system\attachment\SystemAttachmentServices;
use crmeb\basic\BaseJobs;
use crmeb\traits\QueueTrait;

class TaskJob extends BaseJobs
{
    use QueueTrait;

    /**
     * 修改短信发送记录短信状态
     */
    public function modifyResultCode()
    {
        /** @var SmsRecordServices $smsRecord */
        $smsRecord = app()->make(SmsRecordServices::class);
        return $smsRecord->modifyResultCode();
    }

    /**
     * 清除昨日海报
     * @return bool
     * @throws \Exception
     */
    public function emptyYesterdayAttachment()
    {
        /** @var SystemAttachmentServices $attach */
        $attach = app()->make(SystemAttachmentServices::class);
        return $attach->emptyYesterdayAttachment();
    }
}
