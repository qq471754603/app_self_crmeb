<?php


namespace app\jobs;


use app\services\product\product\CopyTaobaoServices;
use crmeb\basic\BaseJobs;
use crmeb\traits\QueueTrait;
use think\facade\Log;

/**
 * 复制商品
 * Class ProductCopyJob
 * @package app\jobs
 */
class ProductCopyJob extends BaseJobs
{
    use QueueTrait;

    /**
     * 下载商品详情图片
     * @param $id
     * @return bool
     */
    public function copyDescriptionImage($id)
    {
        try {
            /** @var CopyTaobaoServices $copyTaobao */
            $copyTaobao = app()->make(CopyTaobaoServices::class);
            $copyTaobao->uploadDescriptionImage((int)$id);
        } catch (\Throwable $e) {
            Log::error('下载商品详情图片失败，失败原因:' . $e->getMessage());
        }
        return true;
    }

    /**
     * 下载商品轮播图片
     * @param $id
     * @return bool
     */
    public function copySliderImage($id)
    {
        try {
            /** @var CopyTaobaoServices $copyTaobao */
            $copyTaobao = app()->make(CopyTaobaoServices::class);
            $copyTaobao->uploadSliderImage((int)$id);
        } catch (\Throwable $e) {
            Log::error('下载商品轮播图片失败，失败原因:' . $e->getMessage());
        }
        return true;
    }
}
