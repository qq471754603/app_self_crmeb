<?php


namespace app\jobs;


use crmeb\basic\BaseJobs;
use crmeb\services\template\Template;
use crmeb\traits\QueueTrait;
use think\facade\Log;
use think\facade\Route;

/**
 * Class TemplateJob
 * @package app\jobs
 */
class TemplateJob extends BaseJobs
{
    use QueueTrait;

    /**
     * @param $type
     * @param $openid
     * @param $tempCode
     * @param $data
     * @param $link
     * @param $color
     * @return bool|mixed
     */
    public function doJob($type, $openid, $tempCode, $data, $link, $color)
    {
        try {
            if (!$openid) return true;
            $template = new Template($type ?: 'wechat');
            $template->to($openid);
            if ($color) {
                $template->color($color);
            }
            if ($link) {

                switch ($type) {
                    case 'wechat':
                        $link =
                            sys_config('site_url') . Route::buildUrl($link)
                                ->suffix('')
                                ->domain(false)->build();
                        break;
                }

                $template->url($link);
            }
            return $template->send($tempCode, $data);
        } catch (\Exception $e) {
            Log::error($e->getMessage());
            return true;
        }
    }

}
