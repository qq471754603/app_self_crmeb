<?php


namespace app\jobs;


use app\services\product\product\StoreProductLogServices;
use crmeb\basic\BaseJobs;
use crmeb\traits\QueueTrait;
use think\facade\Log;

class ProductLogJob extends BaseJobs
{
    use QueueTrait;
    /**
     * @param $type  'visit','cart','order','pay','collect','refund'
     * @param $data
     * @return bool
     */
    public function doJob($type,$data)
    {
        try {
            /** @var StoreProductLogServices $productLogServices */
            $productLogServices = app()->make(StoreProductLogServices::class);
            $productLogServices->createLog($type, $data);
        }catch (\Throwable $e){
            Log::error('写入商品记录发生错误,错误原因:' . $e->getMessage());
        }
        return true;
    }
}
