<?php


namespace app\services\product\product;


use app\dao\product\product\StoreProductVisitDao;
use app\services\BaseServices;

/**
 * Class StoreProductVisitServices
 * @package app\services\product\product
 * @method getUserVisitProductList(array $where, int $page, int $limit) 用户浏览商品足记
 */
class StoreProductVisitServices extends BaseServices
{

    /**
     * StoreProductVisitServices constructor.
     * @param StoreProductVisitDao $dao
     */
    public function __construct(StoreProductVisitDao $dao)
    {
        $this->dao = $dao;
    }
}
