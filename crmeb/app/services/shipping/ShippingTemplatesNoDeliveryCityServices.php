<?php


namespace app\services\shipping;


use app\dao\shipping\ShippingTemplatesNoDeliveryCityDao;
use app\services\BaseServices;

/**
 * 不送达和城市数据连表业务处理层
 * Class ShippingTemplatesNoDeliveryCityServices
 * @package app\services\shipping
 * @method getUniqidList(array $where, bool $group) 获取指定条件下的包邮列表
 */
class ShippingTemplatesNoDeliveryCityServices extends BaseServices
{
    /**
     * 构造方法
     * ShippingTemplatesNoDeliveryCityServices constructor.
     * @param ShippingTemplatesNoDeliveryCityDao $dao
     */
    public function __construct(ShippingTemplatesNoDeliveryCityDao $dao)
    {
        $this->dao = $dao;
    }
}
