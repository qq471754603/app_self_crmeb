<?php


namespace app\services\other;


use app\dao\other\CategoryDao;
use app\services\BaseServices;
use crmeb\traits\ServicesTrait;

/**
 * Class CategoryServices
 * @package app\services\other
 */
class CategoryServices extends BaseServices
{

    use ServicesTrait;

    protected $cacheName = 'crmeb_cate';

    /**
     * CategoryServices constructor.
     * @param CategoryDao $dao
     */
    public function __construct(CategoryDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * 获取分类列表
     * @param array $where
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function getCateList(array $where = [], array $field = ['*'])
    {
        [$page, $limit] = $this->getPageValue();
        $data = $this->dao->getCateList($where, $page, $limit, $field);
        array_unshift($data, ['id' => 0, 'name' => '系统话术', 'sort' => 0]);
        $count = $this->dao->count($where);
        return compact('data', 'count');
    }


}
