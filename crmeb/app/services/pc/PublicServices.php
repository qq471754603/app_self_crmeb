<?php

declare (strict_types=1);

namespace app\services\pc;


use app\services\BaseServices;
use app\services\shipping\SystemCityServices;

class PublicServices extends BaseServices
{
    /**
     * 获取城市数据
     * @param int $pid
     * @return mixed
     */
    public function getCity(int $pid)
    {
        /** @var SystemCityServices $city */
        $city = app()->make(SystemCityServices::class);
        $list = $city->getColumn(['parent_id' => $pid, 'is_show' => 1], 'city_id,name');
        return $list;
    }
}
