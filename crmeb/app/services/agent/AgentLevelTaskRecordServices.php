<?php


namespace app\services\agent;

use app\dao\agent\AgentLevelTaskRecordDao;
use app\services\BaseServices;


/**
 * Class AgentLevelTaskRecordServices
 * @package app\services\agent
 */
class AgentLevelTaskRecordServices extends BaseServices
{
    /**
     * AgentLevelTaskRecordServices constructor.
     * @param AgentLevelTaskRecordDao $dao
     */
    public function __construct(AgentLevelTaskRecordDao $dao)
    {
        $this->dao = $dao;
    }
}
