<?php


declare (strict_types=1);

namespace app\services\user;

use app\services\BaseServices;
use app\dao\user\UserUserBrokerageDao;

/**
 * 用户关联佣金
 * Class UserUserBrokerageServices
 * @package app\services\user
 */
class UserUserBrokerageServices extends BaseServices
{

    /**
     * UserUserBrokerageServices constructor.
     * @param UserUserBrokerageDao $dao
     */
    public function __construct(UserUserBrokerageDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * 获取佣金列表
     * @param array $where
     * @param string $field
     * @param string $order
     * @param int $limit
     * @return array
     */
    public function getBrokerageList(array $where, string $field = '*', string $order = '', int $limit = 0)
    {
        if ($limit) {
            [$page] = $this->getPageValue();
        } else {
            [$page, $limit] = $this->getPageValue();
        }
        $list = $this->dao->getList($where, $field, $order, $page, $limit);
        $count = $this->dao->getCount($where);
        return [$count, $list];
    }
}
