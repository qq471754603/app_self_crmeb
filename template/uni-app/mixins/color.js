

export default {
	data() {
		return {
			colorStyle: '',
			colorStatus: ''
		};
	},
	created() {
		this.colorStyle = uni.getStorageSync('viewColor')
		uni.$on('ok', (data, status) => {
			console.log(status)
			this.colorStyle = data
			this.colorStatus = status
		})
	},
	methods: {}
};
