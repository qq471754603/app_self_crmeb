#!/bin/bash

export LANG=zh_CN
master=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
echo ${master}
cd $master

#parentPath=${master%/*}
parentPath=~/Documents/agent/voice_card_agent/self/self_voice_card/web_app
echo ${parentPath}

PROJECT_ARR=("self")
#from=${master}/unpackage/dist/build/app-plus/
from=${master}/unpackage/resources/__UNI__A23AF79/www/

for i in "${PROJECT_ARR[@]}"
do
#    androidPath=${parentPath}/${i}_voice_card_Android-SDK@3.1.18.80859_20210610/${i}_voice_card-android/${i}_voice_card-android/src/main/assets/apps
    androidPath=${parentPath}/${i}_voice_card_Android-SDK@3.96.81954_20231106/${i}_voice_card-android/${i}_new_voice_card/src/main/assets/apps
    iosPath=${parentPath}/${i}_voice_card_iOSSDK@3.1.18.80433_20210609/voice_card_one/voice_card_one/Pandora/apps
    FILE_PATH="manifest.json"
    echo $FILE_PATH
    appid=`jq '.appid' $FILE_PATH | sed 's/\"//g' | sed 's/ //g'`
    echo ${appid}
    full_android="${androidPath}/${appid}/www"
    echo $full_android
#    rm -rf "${master}/dist"
    rm -rf ${androidPath}
#    rm -rf ${iosPath}
    if [ ! -d $full_android ]; then
      mkdir -p "$full_android"
    fi
#     full_ios="${iosPath}/${appid}/www"
#    echo $full_ios
#    if [ ! -d $full_ios ]; then
#      mkdir -p "$full_ios"
#    fi
#    npm run build:app-plus
#    sleep 3
    echo "start sync"
    cp -r ${from} ${full_android}
#    cp -r ${from} ${full_ios}
    echo "end sync $(date +"%Y%m%d%H%M%S")"

done
