#!/bin/bash

export LANG=zh_CN
master=$( cd "$(dirname "${BASH_SOURCE[0]}")" ; pwd -P )
echo ${master}
#PROJECT_ARR=("b.hxsinfo.com" "yy.lianhengkj.com" "my.17tao8.top" "my10642.cn" "wl.quanchuang365.cn" "xu1.hxljz.top" "xs.xiaokang.icu" "web.xh" "nkw")

#PROJECT_ARR=("lh")
PROJECT_ARR=("self")
#cli_version=3.98
#cli_version=4.15
#cli_version=4.23
cli_version=4.36
cli="/Applications/HBuilderX${cli_version}.app/Contents/MacOS/cli"
#build_platform=H5
#build_platform=RESOURCE
#build_platform=WGT
build_platform=APP
#build_platform=RE_SIGN_APP
build_account=haokeyunvip@163.com
build_password=112233aA!
#if [ "cli_version" == "4.15 "];then
$cli user login --username $build_account  --password $build_password
#fi
IS_INC_VERSION=0
IS_SYNC_FILE=1
#shouji_platform=android
shouji_platform=ios
export ROOT=~/Documents/agent/voice_card_agent/
for i in "${PROJECT_ARR[@]}"
do
    config_file="${master}/sync_config/${i}"
    echo $i
    BUILD_PLATFORM="hy"

#    build_config_file="${config_file}/build_app_config.json"
    project_path="${master}/uni-app"

    FILE_PATH="${project_path}/manifest.json"
    appid=`jq '.appid' $FILE_PATH | sed 's/\"//g' | sed 's/ //g'`
    version_code=`jq '.versionCode' $FILE_PATH | sed 's/\"//g' | sed 's/ //g'`
    version_name=`jq '.versionName' $FILE_PATH | sed 's/\"//g' | sed 's/ //g'`
    echo "appid" $appid
    echo "version_code" $version_code
    if [ "$IS_INC_VERSION" == "1" ];then
         #    版本号+1
          version_name_arr=(${version_name//./ })
          version_name_arr[2]=`expr ${version_name_arr[2]} + 1`
          version_name_new=${version_name_arr[0]}.${version_name_arr[1]}.${version_name_arr[2]}  # 版本号+1
          sed -i '' "s/${version_name}/${version_name_new}/g" $FILE_PATH
      #     version加1
          new_version_code=`expr ${version_code} + 1`
          echo "new_version_code" $new_version_code
          sed -i '' "s/${version_code}/${new_version_code}/g" $FILE_PATH
          version_code=$new_version_code
          version_name=$version_name_new
    fi
#    cp -r "${config_file}/Global.js" "${master}/src/common/Global.js"
#    cp -r "${config_file}/manifest.json" "${master}/src/manifest.json"
#    cp -r "${config_file}/pages.json" "${master}/src/pages.json"
#    exit
        echo "build running"
#      $cli app quit
#      sleep 2
      cd $project_path
#      $cli open
      $cli open --path ${project_path}
      sleep 5
#      打开指定路径文件，并跳转到指定行列2:2
      $cli open --file ${project_path}/manifest.json 2:2

      if [ "$build_platform" == "RE_SIGN_APP" ];then
          project_source=${project_path}/unpackage/resources/
          $cli publish --platform APP --type appResource --project $project_path
          current_day=$(date +"%Y%m%d%H%M%S")
          android_resources=$project_path/unpackage/apk_resources_${current_day}
          if [ ! -d "$android_resources" ];then
            mkdir -p $android_resources
          fi
#          apk_file=$master/1076__UNI__A23AF79_huawei_1122114657.apk
          apk_file=/Users/huxiansheng/Downloads/hy_release_v1.0.2_2024_05_31_18_31_48.apk
          java -jar $master/apktool_2.10.0.jar d -f -o $android_resources d $apk_file

          sleep 5
#            更换assets文件夹内容
          rm -rf $android_resources/assets/apps/*
          cp -r  $project_source $android_resources/assets/apps/
          sign_apk_path=$project_path/unpackage/sign_apk_dir_${current_day}
          if [ ! -d "$sign_apk_path" ];then
              mkdir -p $sign_apk_path
          fi
          no_sign_apk=$sign_apk_path/${BUILD_PLATFORM}_${i}_${cli_version}_${build_platform}_${version_code}_${appid}_${current_day}"_no_sign.apk"
          java -jar $master/apktool_2.10.0.jar b $android_resources -o $no_sign_apk
          name1="voice_card"
          aliaskey="rootca"
          build_app_path=$sign_apk_path/"${i}_${cli_version}_${build_platform}_${version_code}_${appid}_${current_day}-release.apk"
          keystore_path="${config_file}/android/${name1}.keystore"
          ~/Library/Android/sdk/build-tools/30.0.2/apksigner sign --ks ${keystore_path} --ks-pass pass:${name1} --ks-key-alias ${aliaskey} --key-pass pass:${name1} --out ${build_app_path} ${no_sign_apk}
          echo ${build_app_path}
          sleep 5
          echo "re sign success"
          cp -r $build_app_path ~/Downloads/
          if [ $? -eq 0 ];then
            rm -rf $sign_apk_path
            rm -rf $android_resources
          fi
          exit
      fi

      if [ "$build_platform" == "RESOURCE" ];then
        # 生成本地打包App资源,云打包用不上
        $cli publish --platform APP --type appResource --project $project_path
  ###      直接复制一份到安卓工程一份
        android_resources_path=$ROOT/${i}/self_voice_card_app/self_voice_card_Android-SDK@3.96.81954_20231106/self_voice_card-android/self_voice_card-android/src/main/assets/apps/
        rm -rf $android_resources_path/*
        cp -r $project_path/unpackage/resources/$appid  $android_resources_path
      fi
      if [ "$build_platform" == "WGT" ];then

      # 导出wgt包
#      wgt_name="${i}_${cli_version}_APP_WGT_${version}_${appid}_$(date +"%Y%m%d%H%M%S").wgt"
#      $cli publish --platform APP --type wgt --project $project_path --path ${master} --name $wgt_name
#      cp -r $master/$wgt_name  ~/Downloads/
        FILE_NAME="${i}_${cli_version}_${build_platform}_${version_code}_${appid}_$(date +"%Y%m%d%H%M%S").wgt"
        $cli publish --platform APP --type wgt --project $project_path --path ${master} --name $FILE_NAME
        cp -r $master/${FILE_NAME} ~/Downloads/
        if [ -f "$master/${FILE_NAME}" ];then
          if [ "$shouji_platform" == "android" ];then
            AccessKey=RRr0IE0TOm3ryC-oW-w3mKVjGjZWX8p9wLUXGR18
            SecretKey=OPh6mwLRtXCJBWgxz1HAZezZqKYp4ObSA6Drnb8R
            Bucket=olink
            domain=http://cc.lianhengkj.com
            Key="${i}_voice_card_app_hot/${FILE_NAME}"
            qshell user add ${AccessKey} ${SecretKey} ${Bucket}
            qshell fput ${Bucket} ${Key} $master/${FILE_NAME}
            year_month_day=$(date +"%Y-%m-%d %H:%M:%S")
            app_file=${domain}/${Key}
            remark='优化体验..'
            sql="INSERT INTO v_version (type, version_no, wgt_version, content, download_url, location_url, hot_update_url, status, is_force, create_time, update_time) VALUES (1, '${version_name}', '${version_name}', '${remark}', 'http://self.haomifi.com/download/index', 'https://pps.1064m2m.cn/app_hot/self_4.29_RE_SIGN_APP_1073___UNI__A23AF79_20241122142556-release.apk', '${app_file}', 1, 0, '${year_month_day}', '${year_month_day}');
                 INSERT INTO v_version (type, version_no, wgt_version, content, download_url, location_url, hot_update_url, status, is_force, create_time, update_time) VALUES (2, '${version_name}', '${version_name}', '${remark}', 'https://apps.apple.com/cn/app/%E5%8F%B7%E6%98%93/id6471194413', '', '${app_file}', 1, 0, '${year_month_day}', '${year_month_day}');"
            #          生成执行sql
            echo $sql > ~/Downloads/sql.txt
            open ~/Downloads/sql.txt
            echo $sql
          fi
        else
          echo "wgt not exist"
          exit
        fi
      fi
#      exit
      if [ "$build_platform" == "APP" ];then
        sed -i "" "s/首页/号易/g" $project_path/manifest.json
      # 云打包App
#      $cli pack --config $build_config_file
          platform=${shouji_platform}

#            --safemode 	打包方式是否为安心打包,默认值false,true安心打包,false传统打包
          #--iscustom 	是否使用自定义基座 默认值false, true自定义基座 false自定义证书
           safemode=false
           packagename=vip.lwhk.my
           if [ "$shouji_platform" == "android" ];then
             certalias=rootca
             certpassword=voice_card
             certfile=${master}/sync_config/${i}/android/voice_card.keystore
  #           安卓平台要打的渠道包,取值有"google","yyb","360","huawei","xiaomi","oppo","vivo"，如果要打多个逗号隔开
             channels="huawei"
  #           --android.androidpacktype 	安卓打包类型 默认值0,0 使用自有证书 1 使用公共证书 2 使用DCloud老版证书
            $cli pack --project ${project_path} --platform ${platform} --safemode ${safemode} --android.packagename ${packagename} \
            --iscustom false --splashads false  --rpads false --pushads false \
            --android.androidpacktype 0 --android.channels ${channels} \
            --android.certfile ${certfile} \
            --android.certalias ${certalias} \
            --android.certpassword ${certpassword}
          fi
          if [ "$shouji_platform" == "ios" ];then
#            1253233682@qq.com
            #Jqk188741
             $cli pack --project ${project_path} --platform ${platform} --safemode ${safemode} --ios.bundle ${packagename} \
             --iscustom false --splashads false  --rpads false --pushads false \
             --ios.supporteddevice iPhone,iPad --ios.isprisonbreak false 	\
             --ios.profile ${master}/sync_config/${i}/ios/dis_hy_profiles.mobileprovision \
             --ios.certfile ${master}/sync_config/${i}/ios/dis_hy.p12 \
             --ios.certpassword hy_2024

          fi
          exit
      fi
      if [ "$build_platform" == "H5" ];then
        sed -i "" "s/号易/首页/" $project_path/manifest.json

        # 打包H5
        $cli publish --platform h5 --project $project_path
        cd $project_path/unpackage/dist/build
        mv web h5
        FILE_NAME="${i}_${cli_version}_h5_${version_name}_${appid}_$(date +"%Y%m%d%H%M%S").tar.gz"
        build_file_full_path=$master/$FILE_NAME
        tar -zcvf $build_file_full_path h5
        cp -r $build_file_full_path ~/Downloads/
        cd $master
  #      同步到服务器
        HOST_INFO=host.info
        for IP in $(awk '/^[^#]/{print $1}' $HOST_INFO);
        do
          USER=$(awk -v ip=$IP 'ip==$1{print $2}' $HOST_INFO)
          PORT=$(awk -v ip=$IP 'ip==$1{print $3}' $HOST_INFO)
          PASS=$(awk -v ip=$IP 'ip==$1{print $4}' $HOST_INFO)
          echo $IP $USER $PORT $PASS
          echo -e "\033[34m--------start $USER@$IP:$PORT-----------\033[0m"
          SYNC_TO=/www/wwwroot/web_self_voice_card/dist/build/
          expect -c "
              spawn rsync -e \"ssh -p $PORT\" -avzPh $build_file_full_path $USER@$IP:$SYNC_TO
              expect {
                \"password:\" {send \"$PASS\r\"; exp_continue}
              }
               "
          COMMAND="cd $SYNC_TO;sudo tar -zxvf $FILE_NAME;sudo chown -R www:www $SYNC_TO;sudo chmod -R 755 $SYNC_TO"
          expect -c "
              spawn ssh -p $PORT $USER@$IP
              expect {
                \"yes/no\" {send \"yes\r\"; exp_continue}
                \"password:\" {send \"$PASS\r\"; exp_continue}
                \"$USER@*\" {send \"$COMMAND\r exit\r\"; exp_continue}
              }
               "
          echo -e "\033[36m --------end $USER@$IP:$PORT-----------\033[0m"
      #   sleep 1
       done
      fi
    echo 'tar end'
done
